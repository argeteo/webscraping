import sys
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from PyQt4.QtWebKit import *
from lxml import html
from bs4 import BeautifulSoup

#Take this class for granted.Just use result of rendering.
class Render(QWebPage):
    def __init__(self, url):
        self.app = QApplication(sys.argv)
        QWebPage.__init__(self)
        self.loadFinished.connect(self._loadFinished)
        self.mainFrame().load(QUrl(url))
        self.app.exec_()

    def _loadFinished(self, result):
        self.frame = self.mainFrame()
        self.app.quit()

url = "https://www.amazon.it/gp/goldbox/"
r = Render(url)
result = r.frame.toHtml()
#This step is important.Converting QString to Ascii for lxml to process
#archive_links = html.fromstring(str(result))

pagina=BeautifulSoup(result,"html.parser")
titles=pagina.find_all("a",{"id":"dealTitle"})
prices=pagina.find_all("span",{"class":"a-size-medium a-color-base inlineBlock unitLineHeight"})
print(len(prices))
#for i in el:
#    print(i.text)
#    print(i['href'])
